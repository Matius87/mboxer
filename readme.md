# WoW Classic dualboxing helper for Linux clients

### This is work in progress and has still some major issues, like loosing master window and then beeing unable to broadcast keys to clients

1. MBoxer requires you to run your World Of Warcraft Classic instances in Linux with Wine and use Windowed (virtual desktop) mode.
You can set that in Lutris through 
```
Configuration -> Runner options -> Windowed
```

2. Launch all you're WoW instances.

3. Run mboxer, execute mboxer:
```
./mboxer.py
```

4. Point your mouse to master WoW screen within 2 seconds.

You're ready to go!


**How to configure WoW clients:**

1. Set F12 to run macro:
```
/cleartarget
/assist [Mastercharacter]
```

2. Create follow macro 
 - Set F10 to follow macro:
```
/follow [Mastercharacter]
```
 - or with EMA addons:
```
/ema-follow master
```

3. Bind key F11 to interact with target
```
Keybindings->Interaction->Interact with target
```

It's generally good idea to use macros on non master toons to make sure you target same target as your master character.

