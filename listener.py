from pynput import mouse, keyboard
import time


class MListener(object):

    def __init__(self, sender, instances, debug=False):
        self.sender = sender
        self.instances = instances
        self.masterid = self.instances.master_id()

        self.keyboardmapping = {
            keyboard.Key.f1: "F1",
            keyboard.Key.f2: "F2",
            keyboard.Key.f3: "F3",
            keyboard.Key.f4: "F4",
            keyboard.Key.f5: "F5",
            keyboard.Key.f6: "F6",
            keyboard.Key.f7: "F7",
            keyboard.Key.f8: "F8",
            keyboard.Key.f9: "F9",
            keyboard.Key.f10: "F10",
            keyboard.Key.f11: "F11",
            keyboard.Key.f12: "F12",
            keyboard.Key.space: "space",
            keyboard.Key.esc: "Escape"
        }

        self.keyboardalphanumeric = [
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
            "c", "b", "z", "x", "l", "v", "n", "m"
        ]

        self.dropfollow = [
            "2", "4", "5", "6", "7", "8", "9", "0"
        ]

        self.keyboardignored = [
            keyboard.Key.shift,
            keyboard.Key.ctrl,
            keyboard.Key.alt,
            keyboard.Key.cmd,
        ]

        self.mousebindings = {
            mouse.Button.left: 1,
        }

        self.keysdown = []
        self.pause = False
        self.debug = debug
        # self.mouselocation = {'x': 0, 'y': 0}
        # self.mainwindow = {'x_min': 2560, 'x_max': 5120}

    def start(self):
        print("Starting MBoxer listener threads...")
        self.mouselistener = mouse.Listener(
            on_click=self.on_mouse_click)
        self.mouselistener.start()

        with keyboard.Listener(on_press=self.on_keyboard_press, on_release=self.on_keyboard_release) as self.keyboardlistener:
            self.keyboardlistener.join()

        return True

    def stop(self):
        print("Shutting down MBoxer listeners...")
        self.mouselistener.stop()
        self.keyboardlistener.stop()

    def on_mouse_click(self, x, y, button, pressed):
        if self.pause == False:
            if not pressed:
                # if self.mouselocation['x'] > self.mainwindow['x_min'] and self.mouselocation['x'] < self.mainwindow['x_max']:
                if self.masterid == self.instances.find_window_by_pointing():
                    if self.debug:
                        print("Pressed {}".format(button))
                    if button == mouse.Button.right:
                        time.sleep(.75)
                        self.sender.send_key("F12")
                        time.sleep(.1)
                        self.sender.send_key("F11")
                    elif button == mouse.Button.left:
                        # time.sleep(.7)
                        # self.sender.send_key("F12")
                        if self.debug:
                            print("Mouse 1 pressed")
                    elif button == mouse.Button.button8:
                        self.sender.send_key("F10")
                    elif button == mouse.Button.button9:
                        self.sender.send_key("w")
                else:
                    if self.debug or True:
                        print(
                            "Mouse clicked outside masterid {} at: X: {} Y: {}".format(self.masterid, x, y))

    # def on_mouse_scroll(self, x, y, dx, dy):
    #     if not self.pause:
    #         # if self.mouselocation['x'] > self.mainwindow['x_min'] and self.mouselocation['x'] < self.mainwindow['x_max']:
    #         if self.masterid == self.instances.find_window_by_pointing():
    #             if self.debug:
    #                 print('Scrolled {0} at {1}'.format(
    #                     'down' if dy < 0 else 'up',
    #                     (x, y)))
    #             # if dy > 0:
    #             #     self.sender.send_mouse(4)
    #             # else:
    #             #     self.sender.send_mouse(5)

    # def on_move(self, x, y):
    #     self.mouselocation['x'] = x
    #     self.mouselocation['y'] = y
    #     if self.debug:
    #         print('Pointer moved to {0}'.format(
    #             (self.mouselocation['x'], self.mouselocation['y'])))

    def togglepause(self):
        if self.pause:
            self.pause = False
        else:
            self.pause = True
        print("Toggle pause: {}".format(self.pause))

    def on_keyboard_press(self, key):
        if key == keyboard.Key.pause:
            self.togglepause()

        if self.pause == False:
            # if self.mouselocation['x'] > self.mainwindow['x_min'] and self.mouselocation['x'] < self.mainwindow['x_max']:
            if self.masterid == self.instances.find_window_by_pointing():
                try:
                    if self.debug:
                        print('alphanumeric key {0} pressed'.format(key.char))
                    if key.char in self.keyboardalphanumeric and self.keysdown not in self.keyboardignored and key.char not in self.keyboardignored:
                        self.sender.send_key_press(key.char)
                    # elif key.char == "w":
                    #     self.sender.send_key("F10")
                except AttributeError:
                    if self.debug:
                        print('special key {0} pressed'.format(key))
                    if key in self.keyboardmapping and self.keysdown not in self.keyboardignored and key not in self.keysdown:
                        self.sender.send_key_press(self.keyboardmapping[key])
                    if key not in self.keysdown:
                        self.keysdown.append(key)
                    if self.debug:
                        print("Keys down: {}".format(self.keysdown))
                except KeyError:
                    print("Key {} not mapped!".format(key))

    def on_keyboard_release(self, key):
        if self.pause == False:
            # if self.mouselocation['x'] > self.mainwindow['x_min'] and self.mouselocation['x'] < self.mainwindow['x_max']:
            if self.masterid == self.instances.find_window_by_pointing():
                try:
                    if self.debug:
                        print('alphanumeric key {0} released'.format(key.char))
                    if key.char in self.keyboardalphanumeric and self.keysdown not in self.keyboardignored and key.char not in self.keyboardignored:
                        self.sender.send_key_release(key.char)
                except AttributeError:
                    if self.debug:
                        print('special key {0} released'.format(key))
                    if key in self.keyboardmapping and self.keysdown not in self.keyboardignored:
                        self.sender.send_key_release(self.keyboardmapping[key])
                    if key in self.keysdown:
                        self.keysdown.remove(key)
                    else:
                        if self.debug:
                            print("Key {} not mapped!".format(key))
                except KeyError:
                    if self.debug:
                        print("Key {} not mapped!".format(key))
