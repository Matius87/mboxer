from time import sleep
import sys
from shlex import quote
from Xlib.display import Display


class WoWInstances(object):
    def __init__(self, execute, ime, name=None, debug=False):
        self.execute = execute
        self.IME_STRING = ime
        self.WINDOW_STRING = name
        self.debug = debug
        print("Point master window for 2 seconds")
        sleep(2)
        self.masterinstance = self.find_window_by_pointing()

    def id_by_pointing(self):
        virtual_desktop = self.find_window_by_pointing()
        if virtual_desktop is not None:
            self.default_ime = self.find_child_window_by_name(
                virtual_desktop, self.IME_STRING)
            if self.default_ime is None:
                print("Cannot find '{}' window for parent window '{}'".format(
                    self.IME_STRING, virtual_desktop))
        else:
            print("Cannot find '{}' virtual desktop".format(virtual_desktop))
            sys.exit()

        return self.default_ime

    def master_id(self):
        return self.masterinstance

    def id_by_name(self):
        self.desktops = self.find_window_by_name(self.WINDOW_STRING)
        imelist = []

        if self.desktops is not None:
            for virtual_desktop in self.desktops:
                if self.debug:
                    print("Checking mainwindow: {} != {}".format(
                        virtual_desktop, self.masterinstance))
                if int(self.masterinstance) != int(virtual_desktop):
                    try:
                        imelist.append(self.find_child_window_by_name(
                            virtual_desktop, self.IME_STRING))
                    except TypeError as e:
                        if self.debug:
                            print("Got TypeError: {}".format(e))
                else:
                    if self.debug:
                        print("Skipping {}, as it is master instance".format(
                            virtual_desktop))
            if len(imelist) < 0:
                print("Cannot find '{}' window for parent window '{}'".format(
                    self.IME_STRING, virtual_desktop))
            else:
                if self.debug:
                    print("Found windows: {}".format(imelist))
        else:
            print("Cannot find '{}' virtual desktop".format(virtual_desktop))
            sys.exit()

        return imelist

    def find_window_by_name(self, name):
        result = self.execute(
            "xdotool search --name {}".format(quote(name)))
        if self.debug:
            print("xdotool search --name {}".format(quote(name)))
        return result

    def find_window_by_pointing(self):
        display = Display()
        window = display.screen().root
        result = window.query_pointer()
        if self.debug:
            print("windowID: {}".format(result.child.id))
        return result.child.id

    def find_child_window_by_name(self, parent, name):
        result = self.execute(
            "xwininfo -children -id {} | grep {}".format(quote(str(parent)), quote(name)))
        if self.debug:
            print("Result: {}".format(result))
        if result is not None:
            result = result[0]
        else:
            result = self.execute(
                "xwininfo -children -id {}".format(quote(str(parent))))
            result = result[0]

        return result
