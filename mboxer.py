#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author : Intoxx

import signal
import sys
import listener
import sender
import instances
import subprocess


def service_shutdown(signum, frame):
    print('Caught signal %d' % signum)
    raise ServiceExit


def execute(cmd):
    '''
    Execute a given command @cmd in a shell
    Return a list with the splitted result if there is any
    Else it will return None
    '''
    result = None

    try:
        out = subprocess.check_output(cmd, encoding="UTF-8", shell=True)
    except subprocess.CalledProcessError as error:
        print("Error: {}".format(error))
        print("Shutting down")
        sys.exit(1)
    else:
        out = out.split()  # Clear some whitespace
        result = out if len(out) > 0 else None
    return result


def main():
    IME_STRING = '"Default IME": ("wowclassic.exe" "Wine")'
    WINDOW_STRING = "WineDesktop - Wine desktop"

    debugSender = False
    debugListener = False
    debugInstances = False

    signal.signal(signal.SIGTERM, service_shutdown)
    signal.signal(signal.SIGINT, service_shutdown)

    minstances = instances.WoWInstances(
        execute=execute, ime=IME_STRING, name=WINDOW_STRING, debug=debugInstances)

    msender = sender.Sender(
        execute=execute, ime=minstances.id_by_name(), debug=debugSender)

    mboxer = listener.MListener(
        sender=msender, instances=minstances, debug=debugListener)

    try:
        mboxer.start()
    except:
        mboxer.stop()
        sys.exit(0)


if __name__ == '__main__':
    main()
