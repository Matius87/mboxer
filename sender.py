import subprocess


class Sender:

    def __init__(self, execute, ime, debug=False):
        self.execute = execute
        self.default_ime = ime
        self.debug = debug

    def send_mouse(self, key):
        if self.debug:
            print("Sending mouse key {}".format(key))
        if type(self.default_ime) is list:
            for ime in self.default_ime:
                self.execute(
                    "xdotool click --window {} {}".format(ime, key))
                if self.debug:
                    print(
                        "xdotool click --window {} {}".format(ime, key))
            return True
        elif type(self.default_ime) is int:
            self.execute(
                "xdotool click --window {} {}".format(self.default_ime, key))
            return True
        else:
            return False

    def send_key_press(self, key):
        if self.debug:
            print("Sending key {} down".format(key))

        if type(self.default_ime) is list:
            for ime in self.default_ime:
                self.execute(
                    "xdotool keydown --window {} {}".format(ime, key))
                if self.debug:
                    print(
                        "xdotool keydown --window {} {}".format(ime, key))
            return True
        elif type(self.default_ime) is int:
            self.execute(
                "xdotool keydown --window {} {}".format(self.default_ime, key))
            return True
        else:
            return False

    def send_key_release(self, key):
        if self.debug:
            print("Sending key {} up".format(key))
        if type(self.default_ime) is list:
            for ime in self.default_ime:
                self.execute(
                    "xdotool keyup --window {} {}".format(ime, key))
            return True
        elif type(self.default_ime) is int:
            self.execute(
                "xdotool keyup --window {} {}".format(self.default_ime, key))
            return True
        else:
            return False
        return True

    def send_key(self, key):
        if self.debug:
            print("Sending key {}".format(key))
        if type(self.default_ime) is list:
            for ime in self.default_ime:
                self.execute(
                    "xdotool key --window {} {}".format(ime, key))
                if self.debug:
                    print(
                        "xdotool key --window {} {}".format(ime, key))
            return True
        elif type(self.default_ime) is int:
            self.execute(
                "xdotool key --window {} {}".format(self.default_ime, key))
            return True
        else:
            return False
